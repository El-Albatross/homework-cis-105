
#Tax calculator program
#Program takes an user input (in the form of an integer)
#Program calculates 20% of the users input and displays it as "tip"
#Program then calculates 6% of the users input and displays it as  "Maryland Tax"
#Then the program adds the original input, tip, and tax into a final price "Total"

print("Welcome to Pizza Pizza")
print('----------------------')

#user input, including feedback
price = float(input("Type in price of meal here: "))
print("Price:",price)

#define variables
tip_percentage = 0.20
tax_rate = 0.06


#calculate tax: calculate 6% of price variable
tax = round(price*tax_rate, 2)

#display tax
print("6% sales tax:",tax)
#add tax to price
print("Cost with tax:",round(tax+price, 2))

#calculate tip: calculate 20% of price variable, while rounding it to two decimal places (since 1 cent
#is the lowest value that you can have in money
tip = round(price*tip_percentage, 2)
#display tip
print("20% tip:", tip)

#add tip to price and calculate total
print("Total:",round(tax+price+tip, 2))

quit()





