#farenheight to celcius converter porgram
#--------------------------------
#Program takes users input and converts the farenheight temperature to celsius

#Header that describes program to the user
print('Fahrenheit to Celsius Converter: Version 1')
print('--------------------------')

#input for the user
temp_f = float(input("Insert Fahrenheit temperature here:"))

#calculations for the conversions
temp_c=(5/9)*(temp_f-32)

#this code rounds the temperature to two decimal places
rounded = round(temp_c, 2)

#displaying the final answer
print(rounded,"degrees Celsius")

quit()