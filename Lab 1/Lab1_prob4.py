#currency converter
#This program converts US dollars to South Korean won.
#Takes user input (a number) and multiplies the exchange rate factor
#Prints the result out at the end.

#header

print("USD to KRW converter (only calculates from USD")
print("--------------------")

#defines usd as user input
usd=float(input("USD: "))

#defines exchange rate
exchange_rate=1090.00
#the calculation

krw= usd*exchange_rate

print(u"\u20A9",round(krw, 2))

exit()