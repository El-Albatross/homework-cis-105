
#this program uses turtle graphics to draw the olympic rings
#imports the turtle library
import turtle

#for the blue ring
#sets color
turtle.color("blue")

#"lifts the pen (it's not drawing)
turtle.penup()
#sets starting position on the screen for the pen
turtle.goto(-110, -25)
#puts "the pen to the paper" starts drawing
turtle.pendown()
#creates a circle
turtle.circle(45)

#this repeats for the rest of the program until all the rings are drawn

turtle.color("black")
turtle.penup()
turtle.goto(0, -25)
turtle.pendown()
turtle.circle(45)

turtle.color("red")
turtle.penup()
turtle.goto(110, -25)
turtle.pendown()
turtle.circle(45)

turtle.color("yellow")
turtle.penup()
turtle.goto(-55,-75)
turtle.pendown()
turtle.circle(45)

turtle.color("green")
turtle.penup()
turtle.goto(55,-75)
turtle.pendown()
turtle.circle(45)
turtle.done()

exit()