# Counting Numbers to a file
# this program will ask the user how 
# many numbers to write to a file that will
# be automatically opened

# defines the main function
def main():
    # calls the intro function
    intro()
    # defines the how many variable to be what the menu function returns
    how_many = menu()
    # opens counting.txt file and puts it on write mode
    counting_file = open('counting.txt', 'w')
    # for loop set to print numbers from 1 to the how_many number that the user set
    for count in range(1,how_many+1):
        # write method writes the count along with a new line for each new number
        # count must be set as a string value
        counting_file.write(str(count) + '\n')
    # prints confirmation for the user
    print('Numbers 1-',how_many,'written to counting.txt')

# Information for the user
def intro():
    print('Counting Numbers Editor')
    print('=======================')
    print('Type in a number and the program will')
    print('count up to it and write all the numbers')
    print('down in a text file.')
    print('---------------------')
    print('')

def menu():
    # ask for user input to see how far to count
    how_many = int(input('How far would you like to count?: '))
    # returns the input to the main function
    return how_many

# calls the main function
main()