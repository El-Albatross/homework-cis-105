# Read/sum/average program
# Program will read/display the numbers off of counting.txt
# and return the sum of all the numbers as well as the 
# averages

# defines the main function
def main():
    # calls the user information function
    info()
    # opens the counting.txt file and puts it in read mode
    summary_file = open('counting.txt','r')
    # sets the accumulator summary equal to zero
    summary = 0
    # sets the count meter to zero
    count = 0
# for loop runs through each line in the file
    for line in summary_file:
        # this adder variable turns each line from a string into an integer 
        adder = int(line)
        # each loop adds 1 to the count
        count += 1
        # each loop adds the adder (current line) to the total
        summary += adder
        # this prints the current line 
        print(adder)
    # close the file
    summary_file.close()
    # prints the sum total and the average
    print('Sum Total: ',summary)  
    print('Average  :',summary/count)

# information function
def info():
    print('Sum/Average of File program')
    print('===========================')
    print('''This program reads the file
"counting.txt" and displays the results.
The numbers are then added up and averaged.''')
# this input pauses the program until the user presses enter
    input('Press enter to continue:')
    print('---------------')
# calls main function
main()