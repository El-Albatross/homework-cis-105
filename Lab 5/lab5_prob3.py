# This is Problem 2 improved

def main():
    # calls the user information function
    info()
    # start the try-except statment
    try:
        # opens the counting.txt file created in problem 1
        summary_file = open('counting.txt','r')
    # sets the accumulator summary equal to zero
        summary = 0
    # sets the count meter to zero
        count = 0
# for loop runs through each line in the file
        for line in summary_file:
        # this adder variable turns each line from a string into an integer 
            adder = int(line)
        # each loop adds 1 to the count
            count += 1
        # each loop adds the adder (current line) to the total
            summary += adder
        # this prints the current line 
            print(adder)
    # close the file
        summary_file.close()
    # prints the sum total and the average
        print('Sum Total: ',summary)  
        print('Average  :',summary/count)
    except IOError:
        print('An error occured trying to read the file')
    except ValueError:
        print('Invalid data found in the file.')
    except:
        print('An error occured')

# information function
def info():
    print('Sum/Average of File program')
    print('===========================')
    print('''This program reads the file
"counting.txt" and displays the results.
The numbers are then added up and averaged.''')
# this input pauses the program until the user presses enter
    input('Press enter to continue:')
    print('---------------')
# calls main function
main()