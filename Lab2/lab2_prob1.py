# Month of the year program
# Program asks user for a number between 1-12 and then displays the corresponding month of the year.
# If user inputs invalid number, it will display an error and rerun the previous input prompt.

print("Month of the Year Program")
print("------------------------")
print("Type in a number and the corresponding month will be displayed!")

# user input for the number
number = input("Type in a (whole) number between 1-12: ")


# 12 if statements that determine the program's output depending on what
# the value that the number equals.
# For whatever reason, this wouldn't work if I set number input to an integer
# Any non number (string of characters) would crash the program
# It was much easier to have every number set to be a string and it was appropriate since no calculations were made.
if number == "1":
        print("January")
# The elif statement continues the "if" statements, joining them all together and linking them to the final, singular
# else statement.
elif number == "2":
        print("February")
elif number == "3":
        print("March")
elif number == "4":
        print("April")
elif number == "5":
        print("May")
elif number == "6":
        print("June")
elif number == "7":
        print("July")
elif number == "8":
        print("August")
elif number == "9":
        print("September")
elif number == "10":
        print("October")
elif number == "11":
        print("November")
elif number == "12":
        print("December")
else:
        print("Invalid option")
# The else command tells the user that the input they used was invalid







