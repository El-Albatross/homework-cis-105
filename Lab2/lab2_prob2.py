# olympic locations program!
# type in a year that the olympics will occur and the program will return the location of it
# Only valid between the years 2000 to 2026

print('Olympic Games Location Locator-o-matic!')
print('''
Type in a year for the Olympics and this machine 
will tell you where it takes place!''')
print('----only works from years 2000-2026----')
print('')

# here the program asks for the user's input and sets the variable "year" as the input
year = input("Type in Olympic year here (in numbers)")

# here are a series of if-elif-else statements to display the locations of the games
# Each year acts as a string instead of a number, making it easier to make all other inputs invalid
# 
if year == "2000":
    print('It was the Summer Olympics in Sydney, Australia!')
elif year == "2002":
    print('It was the Winter Olympics in Salt Lake City, Utah, in the US')
elif year == "2004":
    print('It was the Summer Olympics in Athens, Greece')
elif year == "2006":
    print('It was the Winter Olympics in Torino, Italy')
elif year == "2008":
    print('It was the Summer Olympics in Beijing, China')
elif year == "2010":
    print('It was the Winter Olympics in Vancouver, Canada')
elif year == "2012":
    print('It was the Summer Olympics in London, UK')
elif year == "2014":
    print('It was the Winter Olympics in Sochi, Russia')
elif year == "2016":
    print('It was the Summer Olympics in Rio de Janeiro, Brazil')
elif year == "2018":
    print('It was the Winter Olympics in Pyeonchang, Best Korea')
elif year == "2020":
    print('It will be the Summer Olympics in Tokyo, Japan')
elif year == "2022":
    print('It will be the Winter Olympics in Beijing, China')
elif year == "2024":
    print('It will be the Summer Olympics in Paris, France')
elif year == "2026":
    print('It has not been announced who will host the 2026 Winter Olympics')
elif year == "2028":
    print('It will be the Summer Olympics in Los Angeles, California in the US')
else:
    print('No Olympics were run that year, try again.')