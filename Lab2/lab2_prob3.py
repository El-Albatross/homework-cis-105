# This progam takes several parameters from the user
# and their hours worked along with their payrate to 
# output their wages for the month

# Introduction/Description
print('Wages Calculator Program')
print('========================')
print('''Overtime wages(over 40 hours a week) are worth 
1.5 times the normal wage. Extra-overtime wages (over 60
hours a week) are worth 2 times the normal wage''')
print('-------------------------------------')
# These variables are important values that eliminate
# "magic" numbers

normal_hours = 40
extra_overtime_threshold = 60
overtime_wage = 1.5
extra_overtime_wage = 2

# This is where the user inputs the number of hours worked
# and their payrate
payrate = float(input('Type in payrate (in $/hr) here:'))
hours = float(input('Type in number of hours worked here: '))

# This long elif statement determines what to do with the
# input depending on the values of the numbers.
if hours <= normal_hours:
    # This calculates the normal wage
    wage1 = hours*payrate
    print('$', round(wage1,2))
elif hours > normal_hours and hours <= extra_overtime_threshold:
    # overtime hours must be calculated to separate
    # them from the normal hourly wage
    overtime = hours-normal_hours
    # calculates initial wage
    wage_initial = normal_hours*payrate
    # calculates (already separated) overtime wage
    overtime_wage1 = overtime*payrate*overtime_wage
    # combines both wages
    wage2 = overtime_wage1+wage_initial
    # displays them
    print('$', round(wage2,2))
elif hours >= extra_overtime_threshold:    
    # this case is more complicated than before
    # two seperations must happen, first with the extra
    # overtime
    extra_overtime1 = hours-extra_overtime_threshold
    # seperates the overtime hours    
    overtime2 = hours-extra_overtime1-normal_hours
    # next comes all three wage calculations
    extra_overtime_wage1 = extra_overtime*(extra_overtime_wage*payrate)
    wage_initial = normal_hours*payrate
    overtime_wage2 = overtime2*payrate*overtime_wage
    # finally, it combines them all into a total
    wage3 = wage_initial+extra_overtime_wage1+overtime_wage2
    print('$',round(wage3,2))
else: 
    print('Invalid Input, try again')
