# Blood pressure program
# Program takes user input to determine "blood pressure category"
# program takes two measurements (systolic and diastolic) and based
# on their value, will print a response of the right category

# print user information
print('Blood Pressure Category Determiner:')
print('===================================')
print('Please measure and round blood pressure in mm Hg')

# Asks for user input while defining the variables
syst = input('Systolic (upper value) measurement: ')
diast = input('Diastolic (lower value) measurement: ')
# this last variable is uneccessary, but saved me time in
# writing the output codes
bpb = str('Blood Pressure Category: ')
# If Elif Else statement to determine category
# the inputs are converted into an integer to be processed
if int(syst) < 120 and int(diast) < 80:
    print(bpb,'NORMAL')
# Here a range function was used. 130 (stop) is not included in the range
elif int(syst) in range(120,130) and int(diast) < 80:
    print(bpb, 'ELEVATED')
# Same as above
elif int(syst) in range(130,139) or int(diast) in range(80,90):
    print(bpb, 'HIGH BLOOD PRESSURE, STAGE 1 (Hypertension)')
elif int(syst) in range(140,180) or int(diast) in range(90,120):
    print(bpb, 'HIGH BLOOD PRESSURE, STAGE 2')
elif int(syst) >= 180 or int(diast) >= 120:
    print(bpb, 'HYPERTENTIVE CRISIS')
# Error message in case of invalid output
else:
    print('Invalid Input')      
