# Problem 1: Tip, Tax, and Total
# this program calculates the total amount of meal purchased at a
# restaurant. User will enter the price of the food and calculate
# tip and sales taxes
#

def main():
    # summons the intro function
    intro()
    # place for a user to input their price to be processed
    money = float(input('Type in price of food item: '))
    # Defines the calculat_tip and  calculate_tax as two functions
    # that are assigned to the variables tip and tax

    # both fucntions have the money variable inputed
    tip = calculate_tip(money)
    tax = calculate_tax(money)
    total = money + tax + tip

    # Prints all the Price data for the user
    print_output(money, tip, tax, total)

# introduction to the program function
def intro():
    print('Tip and Tax calculator for restaurants')
    print('======================================')
    print('Tip is calculated at 20%')
    print('Maryland sales tax is 6%')

# tip calculator function
def calculate_tip(money):
    tip = 0.20*money
    return tip

# tax calculator function
def calculate_tax(mon):
    tax = 0.06*mon
    return tax

# this is the output information function that displays the outputs for the user
def print_output(money, tip, tax, total):
    print('Price of Order: $', format(money, '.2f'))
    print('Tax: $', format(tax, '.2f'))
    print('Tip: $', format(tip,'.2f'))
    print('Total: $', format(total,'.2f'))


# Calls main function
main()

