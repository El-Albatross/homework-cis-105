# Slope of a line cheat calculator
# Given two points, this program can calculate
# the slope of the line connecting the two.

# Defines the main function
def main():
    intro() # calls the introduction function
    # these define all the variables by asking the user for coordinates
    xone = float(input('Initial x value (X-1):'))
    yone = float(input('Initial y value (Y-1):'))
    xtwo = float(input('Final x value (X-2):'))
    ytwo = float(input('Final y value (Y-2):'))
    # The variable rise is set to be whatever the function delta y calculates
    # based on the input variables above
    rise = delta_y(yone,ytwo)
    run = delta_x(xone,xtwo)
    # Here we set the program to print "undefined" if the denominator (the run variable)
    # of the function is zero (since division by zero is impossible)
    if run == 0:
        print('Slope is Undefined (cannot divide by zero)')
    else:
        # calls the slope function while setting the variable sloper to its output
        sloper = slope(rise, run)
        print('Slope =', sloper)

def intro():
    print('''
    Slope Calculator
    ---------------
    Input the any two coordinates on a cartesian plane and
    this machine will calculate the slope.''')

def delta_y(y_one, y_two):
    # This function calculates the change in the height (Y)
    # which is simply the difference between the two points
    delta_y = y_two - y_one
    return delta_y

def delta_x(xone, xtwo):
    # calculates the change in X
    delta_x=xtwo - xone
    return delta_x

def slope(rise, run):
    # calculates the slope, a simple division
    slope = rise/run
    return slope

# calls the main function
main()
