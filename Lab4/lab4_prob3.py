# surface area or volume calculator
# user can pick from a list of options what
# module to use to calculate either the surface area
# or volume of a rectangular prism

# Imports the rectangular prism module 
import rectangular_prism

# Creates global constants to act as options
VOLUME_OPTION = 1
SURFACE_OPTION = 2
EXIT_OPTION = 3

# defines the main function
def main():
    # starts of the variable choice as being equal to zero
    choice = 0
    # starts the loop with EXIT_OPTION being the variable that stops the loop
    while choice != EXIT_OPTION:
        # calls function that displays the menu
        display_menu()
        # user option input
        choice = int(input('Enter Option Here (1, 2, or 3):'))
        # series of if-elif-else statements that determine what the program does with the input
        if choice == VOLUME_OPTION:
            # the user_input function was a way for me not to type it out for every situation
            # this command defines three variables at once
            leng, widt, heig = user_input()
            # prints the solved problem by putting the variables above into the volume function
            # that is in the rectangular_prism module
            print('Volume = ', format(rectangular_prism.volume(leng, widt,heig)), '.2f')
            print('')
        elif choice == SURFACE_OPTION:
            leng, widt, heig = user_input()
            print('Surface Area =', format(rectangular_prism.surface_area(leng,widt,heig)), '.2f')
            print('')
        elif choice == EXIT_OPTION:
            print('Exiting...')
        else:
            print('Error: Invalid selection')
            print('')
# displays the main function
def display_menu():
    print('Rectangular Prism Multi-Function Calculator')
    print('MENU')
    print('1.) Volume of a Rectangular Prism')
    print('2.) Surface Area of a Rectangular Prism')
    print('3.) Exit Program')

# function to get the user's input. Reusable for any other added rectangular prism problems
def user_input():
    length = float(input('Length of prism: '))
    width = float(input('Width of prism: '))
    height = float(input('Height of prism: '))
    # this return command returns three values at once to the main function
    return length, width, height

main()
