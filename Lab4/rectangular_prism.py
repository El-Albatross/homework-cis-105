# Rectangular prism calculations module
# Has several calculations for rectangular
# prisms that a user might need

def volume(length, width, height):
    vol = length*width*height
    return vol

def surface_area(length, width, height):
    surface = 2*(length*width + length*height + width*height)
    return surface
